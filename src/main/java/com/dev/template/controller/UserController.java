package com.dev.template.controller;

import com.dev.template.dto.SignInDTO;
import com.dev.template.dto.SignUpDTO;
import com.dev.template.service.UserService;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.validation.Valid;

@Controller
@RequestMapping("/user")
public class UserController {

	private UserService userService;

	public UserController(UserService userService) {
		this.userService = userService;
	}

	@PostMapping("/sign-up")
	public ResponseEntity signUp(@Valid @RequestBody SignUpDTO signUpDTO) {
		return ResponseEntity
				.status(HttpStatus.OK)
				.contentType(MediaType.APPLICATION_JSON)
				.body(userService.signUp(signUpDTO));
	}

	@PostMapping("/sign-in")
	public ResponseEntity signIn(@RequestBody SignInDTO signInDTO) {
		return ResponseEntity
				.status(HttpStatus.OK)
				.contentType(MediaType.APPLICATION_JSON)
				.body(userService.signIn(signInDTO));
	}

}
