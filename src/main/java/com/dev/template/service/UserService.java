package com.dev.template.service;

import com.dev.template.dto.ResponseDTO;
import com.dev.template.dto.SignInDTO;
import com.dev.template.dto.SignUpDTO;

public interface UserService {

	ResponseDTO signUp(SignUpDTO signUpDTO);
	ResponseDTO signIn(SignInDTO signInDTO);

}
