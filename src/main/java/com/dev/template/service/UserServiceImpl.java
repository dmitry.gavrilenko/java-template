package com.dev.template.service;

import com.dev.template.dto.ResponseDTO;
import com.dev.template.dto.SignInDTO;
import com.dev.template.dto.SignUpDTO;
import com.dev.template.exception.AuthorizeException;
import com.dev.template.model.User;
import com.dev.template.repository.UserRepository;
import org.modelmapper.ModelMapper;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

@Service
public class UserServiceImpl implements UserService {

	private UserRepository userRepository;

	private ModelMapper modelMapper;

	public UserServiceImpl(UserRepository userRepository, ModelMapper modelMapper) {
		this.userRepository = userRepository;
		this.modelMapper = modelMapper;
	}

	@Override
	public ResponseDTO signUp(SignUpDTO signUpDTO) {
		var user = modelMapper.map(signUpDTO, User.class);
		userRepository.save(user);
		return new ResponseDTO(HttpStatus.OK, "User successfully created", HttpStatus.OK.value());
	}

	@Override
	public ResponseDTO signIn(SignInDTO signInDTO) {
		var user = userRepository.findUserByEmail(signInDTO.getEmail())
				.orElseThrow(() ->  new AuthorizeException(HttpStatus.BAD_REQUEST,
						"Wrong email or password",
						HttpStatus.BAD_REQUEST.value()));
		if(!user.getPassword().equals(signInDTO.getPassword())){
			throw new AuthorizeException(HttpStatus.BAD_REQUEST,
					"Wrong email or password",
					HttpStatus.BAD_REQUEST.value());
		}
		return new ResponseDTO(HttpStatus.OK,
				"Sign in successfully", HttpStatus.OK.value());
	}
}
