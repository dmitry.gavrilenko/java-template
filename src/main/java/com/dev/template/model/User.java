package com.dev.template.model;

import javax.persistence.*;

@Entity
@Table(name = "dev_user")
public class User {

	@Id
	@GeneratedValue(strategy =
			GenerationType.IDENTITY)
	private long id;

	private String name;

	private String email;

	private String password;

	private String authority = "ROLE_USER";

	public User() {
	}

	public User(String name, String email, String password, String authority) {
		this.name = name;
		this.email = email;
		this.password = password;
		this.authority = authority;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getAuthority() {
		return authority;
	}

	public void setAuthority(String authority) {
		this.authority = authority;
	}
}
