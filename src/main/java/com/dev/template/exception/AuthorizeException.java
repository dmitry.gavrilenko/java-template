package com.dev.template.exception;

import org.springframework.http.HttpStatus;

public class AuthorizeException extends GlobalException {
	public AuthorizeException() {
	}

	public AuthorizeException(String msg) {
		super(msg);
	}

	public AuthorizeException(HttpStatus status, String msg, int code) {
		super(status, msg, code);
	}
}
