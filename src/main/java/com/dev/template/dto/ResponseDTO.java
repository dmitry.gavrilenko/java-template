package com.dev.template.dto;

import org.springframework.http.HttpStatus;

public class ResponseDTO {

	public HttpStatus status;

	public String message;

	public int code;

	public ResponseDTO() {
	}

	public ResponseDTO(String message) {
		this.message = message;
	}

	public ResponseDTO(HttpStatus status, String message, int code) {
		this.status = status;
		this.message = message;
		this.code = code;
	}

	public HttpStatus getStatus() {
		return status;
	}

	public void setStatus(HttpStatus status) {
		this.status = status;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public int getCode() {
		return code;
	}

	public void setCode(int code) {
		this.code = code;
	}
}
